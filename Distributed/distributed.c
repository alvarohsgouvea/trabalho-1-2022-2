#include <stdio.h>
#include <signal.h>
#include <stdlib.h>
#include <bcm2835.h>
#include <string.h>
#include "cJSON.h"
#include <arpa/inet.h> // inet_addr()
#include <netdb.h>
#include <strings.h> // bzero()
#include <sys/socket.h>
#include <unistd.h> // read(), write(), close()
#include <pthread.h>
#include <semaphore.h>
#include <time.h>
#define MAX 120
#define PORT 10200
#define SA struct sockaddr

int exitFlag = 0;
int presenca = 0;
int TotalPessoas = 0;
int SALA = 1, ALARME_STATUS = 0;
int L_01, L_02, AC, PR, AL_BZ, SPres, SFum, SJan, SPor, SC_IN, SC_OUT, DHT22;
int dist_PORT, central_PORT, sockfd, LightsOn = 0, Emergency = 0;
char dist_IP[20], Central_IP[20];
time_t logClock;
pthread_t id[2];
int thread, *ptr;
FILE* logFile;

sem_t sem1;

// Parser de JSON
FILE *json;

cJSON* IpCentral = NULL;
cJSON* portaCentral = NULL;
cJSON* IpDistribuido = NULL;
cJSON* portaDistribuido = NULL;
cJSON* sala_json = NULL;

cJSON* file_parser = NULL;
char buffer[5050];

void turnoff(int portas[]){
    for(int i = 0; i < 5; i++){
        bcm2835_gpio_write(portas[i], LOW);
    } 
    if(Emergency == 1){
        delay(5000);
        bcm2835_gpio_write(portas[5], LOW);
    }

    fclose(logFile);
}

void configura_pinos(){

    // Define botão como entrada
    bcm2835_gpio_fsel(SPres, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SFum, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SJan, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SPor, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SC_IN, BCM2835_GPIO_FSEL_INPT);
    bcm2835_gpio_fsel(SC_OUT, BCM2835_GPIO_FSEL_INPT);
    // Configura entrada do botão como Pull-down
    bcm2835_gpio_set_pud(SPres, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SFum, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SJan, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SPor, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SC_IN, BCM2835_GPIO_PUD_DOWN);
    bcm2835_gpio_set_pud(SC_OUT, BCM2835_GPIO_PUD_DOWN);

    // Configura pinos dos LEDs como saídas
    bcm2835_gpio_fsel(L_01, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(L_02, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(AC, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(PR, BCM2835_GPIO_FSEL_OUTP);
    bcm2835_gpio_fsel(AL_BZ, BCM2835_GPIO_FSEL_OUTP);
}

void configureButton(char nome[], int porta){
    int disp;
    char listaTags[12][40] = {"Lâmpada 01", "Lâmpada 02", "Projetor Multimidia", "Ar-Condicionado (1º Andar)", "Sirene do Alarme", "Sensor de Presença", "Sensor de Fumaça", "Sensor de Janela", "Sensor de Porta", "Sensor de Contagem de Pessoas Entrada", "Sensor de Contagem de Pessoas Saída", "Sensor de Temperatura e Umidade"};

    for(int i = 0; i < 12; i++){
        if(strcmp(listaTags[i], nome) == 0){
            disp = i+1;
            break;
        }
    }

    switch(disp){
        case 1:
            L_01 = porta;
        break;
        case 2:
            L_02 = porta;
        break;
        case 3:
            PR = porta;
        break;
        case 4:
            AC = porta;
        break;
        case 5:
            AL_BZ = porta;
        break;
        case 6:
            SPres = porta;
        break;
        case 7:
            SFum = porta;
        break;
        case 8:
            SJan = porta;
        break;
        case 9:
            SPor = porta;
        break;
        case 10:
            SC_IN = porta;
        break;
        case 11:
            SC_OUT = porta;
        break;
        case 12:
            DHT22 = porta;
        break;
    }
}

void acionaDisp(char dispositivo[], int estado, int Dispositivos[], char nomes[][20], int lim){
    time_t logClock = time(NULL);
    if(strcmp(dispositivo, "!ALARME") == 0){
        ALARME_STATUS = estado;
    }
    for(int i = 0; i < lim-1; i++){
        if(strcmp(nomes[i], dispositivo) == 0){
            if(i == 2){
                if(estado == 1){
                    bcm2835_gpio_write(Dispositivos[0], HIGH);
                    bcm2835_gpio_write(Dispositivos[1], HIGH);
                    fprintf(logFile, "Lampadas;Ligadas;%d;%s", SALA, ctime(&logClock));
                }else{
                    bcm2835_gpio_write(Dispositivos[0], LOW);
                    bcm2835_gpio_write(Dispositivos[1], LOW);
                    fprintf(logFile,"Lampadas;Desligadas;%d;%s", SALA, ctime(&logClock));
                }
            }else{
                char name4log[128];
                strncpy(name4log, dispositivo+1, strlen(dispositivo)-1);
                name4log[strlen(dispositivo)-1] = '\0';
                if(estado == 1){
                    bcm2835_gpio_write(Dispositivos[i], HIGH);
                    fprintf(logFile,"%s;Ligado;%d;%s", name4log, SALA, ctime(&logClock));
                }else{
                    bcm2835_gpio_write(Dispositivos[i], LOW);
                    fprintf(logFile,"%s;Desligado;%d;%s", name4log, SALA, ctime(&logClock));
                }
            }
        }
    }
}

char *catLevel(int status){
    char temp[5];

    if(status == 0){
        return "Desligado";
    }else if(status == 1){
        return "Ligado   ";
    }
}

void readStatus(){
    char LargeMessage[4096];
    char *status;

    status = malloc(20);
    //Lampada 1 e Dois
    strcpy(LargeMessage, "L1: ");
    status = catLevel(bcm2835_gpio_lev(L_01));
    strcat(LargeMessage, status);

    strcat(LargeMessage, "  |   L2: ");
    status = catLevel(bcm2835_gpio_lev(L_02));
    strcat(LargeMessage, status);
    strcat(LargeMessage, "\n");

    // Ar-Condicionado
    strcat(LargeMessage, "AC: ");
    status = catLevel(bcm2835_gpio_lev(AC));
    strcat(LargeMessage, status);

    // Projetor
    strcat(LargeMessage, "  |   PR: ");
    status = catLevel(bcm2835_gpio_lev(PR));
    strcat(LargeMessage, status);
    strcat(LargeMessage, "\n");

    // Porta
    strcat(LargeMessage, "PT: ");
    if(bcm2835_gpio_lev(SPor) == 1){
        status = "Aberta";
    }else{
        status = "Fechada  ";
    }
    strcat(LargeMessage, status);

    // Janela
    strcat(LargeMessage, "  |   JN: ");
    if(bcm2835_gpio_lev(SJan) == 1){
        status = "Aberta";
    }else{
        status = "Fechada";
    }
    strcat(LargeMessage, status);
    strcat(LargeMessage, "\n\n");

    // Pessoas
    strcat(LargeMessage, "Pessoas na Sala:  ");
    char temp[5];
    sprintf(temp, "%d\n", TotalPessoas);
    strcat(LargeMessage, temp);

    // Enviar Dados Atualizados
    LargeMessage[strlen(LargeMessage)+1] = '\0';
    if(Emergency == 0)
        write(sockfd, LargeMessage, sizeof(LargeMessage));
    else 
        write(sockfd, "?? FOGO ??", 4096);
    bzero(LargeMessage, 4096);
}

void startup_config(int sala, char **argv){
  //Open file and store data
    char filename[128];
    strcpy(filename, argv[1]);
    strcat(filename, ".json");
    
    FILE *json;
    if(access(filename, 0) == 0)
        json = fopen(filename, "r");
    else{printf("FILE NOT FOUND\n");exit(0);}
        
    fread(buffer, 5048, 1, json);
    //Parse
    file_parser = cJSON_Parse(buffer);
    if(file_parser == NULL){
    printf("Parse Failed!\n");
    exit(-1);
    }
    // Data array
    cJSON* output = NULL;
    cJSON* input = NULL;
    cJSON* gpio = NULL;
    cJSON* tag = NULL;
    cJSON* dht = NULL;
    cJSON* dispositivo = NULL;


    //Extract data
    IpCentral = cJSON_GetObjectItem(file_parser, "ip_servidor_central");
    portaCentral = cJSON_GetObjectItem(file_parser, "porta_servidor_central");
    IpDistribuido = cJSON_GetObjectItem(file_parser, "ip_servidor_distribuido");
    portaDistribuido = cJSON_GetObjectItem(file_parser, "porta_servidor_distribuido");

    // Valores
    strcpy(Central_IP, IpCentral->valuestring);
    central_PORT = portaCentral->valueint;
    strcpy(dist_IP, IpDistribuido->valuestring);
    dist_PORT = portaDistribuido->valueint;

    //Portas GPIO
    output = cJSON_GetObjectItem(file_parser, "outputs");

    for(int i = 0; i < cJSON_GetArraySize(output); i++){
        dispositivo = cJSON_GetArrayItem(output, i);
        tag = cJSON_GetObjectItem(dispositivo, "tag");
        gpio = cJSON_GetObjectItem(dispositivo, "gpio");
        configureButton(tag->valuestring, gpio->valueint);
    }

    input = cJSON_GetObjectItem(file_parser, "inputs");
    for(int i = 0; i < cJSON_GetArraySize(input); i++){
        dispositivo = cJSON_GetArrayItem(input, i);
        tag = cJSON_GetObjectItem(dispositivo, "tag");
        gpio = cJSON_GetObjectItem(dispositivo, "gpio");
        configureButton(tag->valuestring, gpio->valueint);
    }

    dht = cJSON_GetObjectItem(file_parser, "sensor_temperatura");
    dispositivo = cJSON_GetArrayItem(dht, 0);
    tag = cJSON_GetObjectItem(dispositivo, "tag");
    gpio = cJSON_GetObjectItem(dispositivo, "gpio");


    fclose(json);
}

void message(){
    char buff[MAX];
    int i, config;
    int listaSala[6] = {L_01, L_02, 0, AC, PR, AL_BZ};
    char ListaNomes[6][20] = {"!Lampada1", "!Lampada2", "!Lampadas", "!Ar-Condicionado", "!Projetor", "!ALARME"};
    // infinite loop for chat
    for (;;) {
        bzero(buff, MAX);
   
        // read the message from client and copy it in buffer
        do{
            read(sockfd, buff, sizeof(buff));
        }while(buff[0] != '!');
        char objeto[20], temp[5];

        //strncpy(buff, objeto, sizeof(buff)-2);
        strncpy(objeto, buff, strlen(buff)-2);
        strncpy(temp, buff+(strlen(buff)-2), 2);
        objeto[strlen(buff)-2] = '\0';
        
        // if msg contains "Exit" then server exit and chat ended
        if (strcmp("!exit", buff) == 0) {
            printf("Client Exit...\n");
            write(sockfd, "END", 128);
            sem_post(&sem1);
            exitFlag = 1;
            turnoff(listaSala);
            exit(0);
        }else if (strcmp("!listening", buff) == 0 && Emergency == 0){
            readStatus(sockfd);
        }else if(buff[0] == '!'){
            //Acender dispositivo
            acionaDisp(objeto, atoi(temp), listaSala, ListaNomes, 6);
        }
        bzero(buff, MAX);

        strcpy(buff, "return\n");
        // and send that buffer to client
        
        write(sockfd, buff, sizeof(buff));
    }
}

void* interrompe(void *p){
    sem_wait(&sem1);
    int flag = 0, timer = 0, entrada, saida;
    char NOTIFICATION[128];
    while(flag == 0){
        if((bcm2835_gpio_lev(SJan) == 1 || bcm2835_gpio_lev(SPor) == 1 || bcm2835_gpio_lev(SPres) == 1) && ALARME_STATUS == 1){
            Emergency = 1;
            strcpy(NOTIFICATION, "!! INTRUSO !!");
            write(sockfd, NOTIFICATION, sizeof(NOTIFICATION));
            bcm2835_gpio_write(AL_BZ, HIGH);
            delay(1000);

            flag = 1;
            return p;
        }else if(bcm2835_gpio_lev(SFum) == 1){
            Emergency = 1;
            strcpy(NOTIFICATION, "?? FOGO ??");
            write(sockfd, NOTIFICATION, sizeof(NOTIFICATION));
            bcm2835_gpio_write(AL_BZ, HIGH);
            delay(1000);
            // bcm2835_gpio_write(AL_BZ, LOW);

            flag = 1;
            return p;
        }else if(bcm2835_gpio_lev(SPres) == 1 && ALARME_STATUS == 0 && LightsOn == 0){
            bcm2835_gpio_write(L_01, HIGH);
            bcm2835_gpio_write(L_02, HIGH);
            delay(15000);
            bcm2835_gpio_write(L_01, LOW);
            bcm2835_gpio_write(L_02, LOW);
            LightsOn = 1;
        }if(bcm2835_gpio_lev(SPres) == 0 && LightsOn == 1){
            LightsOn = 0;
        }
        //Contagem de pessoas
        entrada = bcm2835_gpio_lev(SC_IN);
        saida = bcm2835_gpio_lev(SC_OUT);
        delay(5);
        if(entrada == 1 && bcm2835_gpio_lev(SC_IN) == 0){
            TotalPessoas++;
        }
        if(saida == 1 && bcm2835_gpio_lev(SC_OUT) == 0){
            TotalPessoas--;
        }
    }
    sem_post(&sem1);
}


int main(int argc, char **argv){
    // Variaveis TCP/IP
    int len;
    struct sockaddr_in servaddr, cli;
    // Abrir arquivo se existir ou criar se nao

    if(access("log.csv", 0) != 0){
        printf("Arquivo nao Existe");
        logFile = fopen("log.csv", "a");
        fprintf(logFile, "Dispositivo;Estado;Sala;Timestamp\n");
    }else{
        logFile = fopen("log.csv", "a");
    }
    // Abre clock
    time(&logClock);

    // Inicializa codigo
    startup_config(1, argv);
    if (!bcm2835_init())
        exit(1);

    configura_pinos();

    // socket create and verification
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        printf("socket creation failed...\n");
        exit(0);
    }
    else
        printf("Socket successfully created..\n");
    
    // Set IP & Port
    struct sockaddr_in localaddr;
    localaddr.sin_family = AF_INET;
    localaddr.sin_addr.s_addr = inet_addr(dist_IP);
    localaddr.sin_port = dist_PORT;  // Any local port will do
    bind(sockfd, (struct sockaddr *)&localaddr, sizeof(localaddr));

    bzero(&servaddr, sizeof(servaddr));
 
    // assign IP, PORT
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(Central_IP);
    servaddr.sin_port = htons(central_PORT);
 
    // connect the client socket to server socket
    if (connect(sockfd, (SA*)&servaddr, sizeof(servaddr))
        != 0) {
        printf("connection with the server failed...\n");
        exit(0);
    }
    else
        printf("connected to the server..\n");
        write(sockfd, "Connection established", 4096);
   
    // Comnunicacao e threads
    sem_init(&sem1, 0, 1);
    pthread_create(&id[0], NULL, interrompe, &thread);

    system("clear");

    message(sockfd);
    
    pthread_join(id[0], (void**)&ptr);
    sem_destroy(&sem1);

    // Fechar Socket e arquivo
    fclose(logFile);
    close(sockfd);
}