#include <arpa/inet.h> // inet_addr()
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h> // bzero()
#include <sys/socket.h>
#include <unistd.h> // read(), write(), close()
#include <time.h>
#include <pthread.h>
#include "cJSON.h"
#include <semaphore.h>
#define MAX 80
#define PORT 10000
#define SA struct sockaddr
int sockfd, client[2];

int SL = 1;
pthread_t id[2];
int thread, statusUpd, *ptr;
char out = 0;
int flag = 0;

sem_t sem1;

cJSON* file_parser = NULL;

char mensagem[4096];

void* statusUpdater(void *p){
    char line[4096];
    while(out == 0){
        write(client[SL-1], "!listening", 4096);
        system("clear");
        if(mensagem[0] == 'L')
            strcpy(line, mensagem);
        printf("Digite 1 para sair\n\n%s\n", mensagem);
        
        sleep(1.5);
    }
}

void* interrompe(void *p){
    sem_wait(&sem1);
    char NOTIFICATION[128];
    while(flag == 0){
        read(client[SL-1], NOTIFICATION, sizeof(NOTIFICATION));
        if(NOTIFICATION[0] == '!'){
            system("clear");
            printf("!! INTRUSO DETECTADO FECHANDO TODOS OS SISTEMAS !!\n");
            write(client[SL-1], "!exit", 1024);
            out = 1;
            flag = 1;
            sleep(5);
            exit(0);
            break;
        }else if(NOTIFICATION[0] == '?'){
            system("clear");
            printf("!! DETECTOR DE FUMACA ATIVADO FAVOR SE RETIRAR DA SALA !!\n");
            write(client[SL-1], "!exit", 1024);
            out = 1;
            flag = 1;
            sleep(5);
            exit(0);
            break;
        }else if(strncmp(NOTIFICATION, "L1", 2) == 0){
            strcpy(mensagem, NOTIFICATION);
        }
    }
    sem_post(&sem1);
    exit(0);
}

int ligar_desligar(){
    int estado;
    system("clear");
    printf("1. Ligar Dispositivo\n2. Desligar Dispositivo\n> ");
    scanf("%d", &estado);
    if(estado < 1 || estado > 2){
        estado = ligar_desligar();
    }
    return estado;
}

void message(int sockfd, int readLast){
    char buff[MAX];

    bzero(buff, sizeof(buff));
    strcpy(buff, mensagem);
    write(sockfd, buff, sizeof(buff));
    bzero(buff, sizeof(buff));
}

void menu(){
    // Variaveis
    int choice;
    int disp = 0;
    int estado;

    system("clear");
    printf("Bem vindo Ao Controle Das Salas!\n\n");
    printf("1. Dispositivos\n2. Verificar Estado da Sala\n3. Sair\n> ");
    scanf("%d", &choice);
    
    switch(choice){
        case 1:
            while(disp < 1 || disp > 6){
                system("clear");
                printf("1. Lampada 1\n2. Lampada 2\n3. Lampadas\n4. Ar-Condicionado\n5. Projetor\n6. Alarme\n> ");
                scanf("%d", &disp);
                switch(disp){
                    case 1:
                        strcpy(mensagem, "!Lampada1");
                        break;
                    case 2:
                        strcpy(mensagem, "!Lampada2");
                        break;
                    case 3:
                        strcpy(mensagem, "!Lampadas");
                        break;
                    case 4:
                        strcpy(mensagem, "!Ar-Condicionado");
                        break;
                    case 5:
                        strcpy(mensagem, "!Projetor");
                        break;
                    case 6:
                        strcpy(mensagem, "!ALARME");
                        break;
                }
                if(disp > 0 && disp < 7){
                    estado = ligar_desligar();
                    if(estado == 1){
                        strcat(mensagem, " 1");
                    }else{
                        strcat(mensagem, " 0");
                    }
                }
            }
            break;
        case 2:
            out = 0;
            pthread_create(&id[1], NULL, statusUpdater, &statusUpd);
            scanf("%d", &out);
            write(client[SL-1], "!NONE", 4096);
            pthread_join(id[1], (void**)&ptr);
        break;
        case 3:
            strcpy(mensagem, "!exit");
            flag = 1;
            message(client[SL-1], 1);
            read(client[SL-1], mensagem, sizeof(mensagem));
            sleep(2);
            exit(0);
        break;
    }
    message(client[SL-1], 1);
    menu();
}
 
int main(int argc, char **argv){
    int sockfd, len;
    struct sockaddr_in servaddr, cli;

    // Ler JSON pra pegar o IP
    char filename[128], buffer[4096];
    strcpy(filename, argv[1]);
    strcat(filename, ".json");
    
    FILE *json;
    if(access(filename, 0) == 0)
        json = fopen(filename, "r");
    
    fread(buffer, 4096, 1, json);
    fclose(json);
    //Parse
    file_parser = cJSON_Parse(buffer);
    if(file_parser == NULL){
        printf("Parse Failed!\n");
        exit(-1);
    }

    cJSON* IP[2];

    //Extract data
    IP[0] = cJSON_GetObjectItem(file_parser, "ip_servidor_central");
    IP[1] = cJSON_GetObjectItem(file_parser, "porta_servidor_central");

    //Criar e verificar socket
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        exit(0);
    }
    bzero(&servaddr, sizeof(servaddr));
    
    // Definir IP
    servaddr.sin_family = AF_INET;
    servaddr.sin_addr.s_addr = inet_addr(IP[0]->valuestring);
    servaddr.sin_port = htons(IP[1]->valueint);
    
    // Bind do socket
    if ((bind(sockfd, (SA*)&servaddr, sizeof(servaddr))) != 0) {
        exit(0);
    }
    
    // Confere se consegue ouvir
    if ((listen(sockfd, 5)) != 0) {
        exit(0);
    }
    len = sizeof(cli);

    system("clear");
    printf("Servidor Criado\n");
    
    // Aceitar cliente
    client[SL-1] = accept(sockfd, (SA*)&cli, &len);
    if (client[SL-1] < 0) {
        exit(0);
    }
    else{
        char connection[4096];
        read(client[SL-1], connection, 4096);
    }
    sem_init(&sem1, 0, 1);
    pthread_create(&id[0], NULL, interrompe, &thread);
 
    // function for chat
    char str[INET_ADDRSTRLEN];
    inet_ntop( AF_INET, &sockfd, str, INET_ADDRSTRLEN );
    menu();

    pthread_join(id[0], (void**)&ptr);
    sem_destroy(&sem1);
 
    // close the socket
    close(sockfd);
}